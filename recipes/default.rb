#Confirm attribute precendence is flowed down to the policy file and attributes can be inherited
Chef::Log.info "Cookbook Attribute from base is ............................. #{node['attribprec']['strvar']['base']}"
Chef::Log.info "Cookbook Attribute from base over ridden in wrapper is ...... #{node['attribprec']['strvar']['base_wr_ov']}"
Chef::Log.info "Cookbook Attribute from base over ridden in policy is ....... #{node['attribprec']['strvar']['base_pol_ov']}"
Chef::Log.info "Cookbook Attribute from wrapper over ridden in policy is .... #{node['attribprec']['strvar']['wrapper_pol_ov']}"

#Determine how directories are handled

directory "/opt/chefdk/test" do
  group "other"
  owner "root"
end

#Files and directories are only taken from the wrappercookbook they completely override the base
remote_directory "/opt/chefdk/test/basedir/" do 
  source "basedir"
  files_owner "root"
  files_group "root"
  files_mode "0755"
  owner "root"
  group "root"
  mode "0755"
  overwrite true
end

#Templates
template "/opt/chefdk/test/templateOverridetest.txt" do
  source "templatefromcb.erb"
  #variables ({:tomcat_log_location => node[:nrpe][:graylog2_log]})
  owner "root"
  group "root"
  mode "0644"
end

# !!! Templates can not be used from basecookbook they must merged into wrapper or called from recipe in basecookbook !!! #
#template "/opt/chefdk/test/templateInheritancetest.txt" do
 # source "basecookbooktemplate.erb"
 #  owner "root"
 # group "root"
 # mode "0644"
#end